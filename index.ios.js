/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 'use strict';

//var React = require('react');
//var ReactNative = require('react-native');
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, NavigatorIOS, View, Image } from 'react-native';
import Alphabet from './components/alphabet'

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  subContainer: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    alignItems: 'center',
    backgroundColor: 'skyblue',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 30,
    marginTop: 65,
  },

  image: {
    width: 217,
    height: 138
  }
});

export default class SampleLoginApp extends Component {
  render() {
    return (
      <NavigatorIOS
        style={styles.container}
        initialRoute={{
          title: 'Kidds App',
          component: Alphabet,
        }}/>
    );
  }
}

AppRegistry.registerComponent('SampleLoginApp', () => SampleLoginApp);
